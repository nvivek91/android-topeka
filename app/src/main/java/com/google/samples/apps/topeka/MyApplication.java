package com.google.samples.apps.topeka;

import android.app.Application;

import com.genie.Genie;

/**
 * Created by vivek on 16/02/16.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate()
    {
        super.onCreate();

        // Initialize the singletons so their instances
        // are bound to the application process.
        Genie.getInstance().init(this, "509ef60d-aaf9-48e4-b56a-1b3a4ec19dd8", "a0aa873c-684b-4e94-adeb-2997f6190222");
//        Genie.getInstance().showAd(this);
    }
}
